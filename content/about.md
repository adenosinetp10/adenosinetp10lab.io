---
title: "About"
date: 2023-07-23T10:17:00+05:30
draft: false
---

Hi,

I hope that you already know my name because you have already read it in the home page.

Well, let me say it again. I'm **Aadhavan**. Now, I'm speaking from experience when I say that having TWO a's sucks!

I'll later explain about this.

I've just now graduated from college. Well, technically not graduated, but finished all my final exams. All I have to get is the Transfer Certificate and Degree. Talking about my degree, I'm an undergraduate in **B.Tech** in the field of **Information Technology**.

My native language is **Tamil/Tamizh (தமிழ்)** commonly spoken in the Southern most state of **India**.

I also know **English** which I'm continuously learning from my school days. I would not say that I'm as proficient as a native, but I'm okay I guess.

## I'll add some stuff later..

Thanks.