---
title: "Projects"
date: 2023-07-23T10:17:05+05:30
draft: false
---

I've done several projects for fun and also some as a part of my college course.

I'll add those later.

But if you want to look at those anyway, then check out my 
[GitLab](https://gitlab.com/adenosinetp10) or
[GitHub](https://github.com/adenosinetp10)


Thanks.